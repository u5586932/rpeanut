package utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

/**
 * Created by Zhiduo Zhang on 4/3/2015.
 */
public class PreferencesGUI extends JDialog {
    Preferences prefs;

    public static final String B_HL_ALL   = "b_hl_all";        //Boolean should hightlight text
    public static final String B_HL_INSTR = "b_hl_instr";      //Boolean should highlight instructions
    public static final String B_HL_REG   = "b_hl_reg";        //Boolean should highlight registers
    public static final String B_HL_HEX   = "b_hl_hex";        //Boolean should highlight hexadecimal
    public static final String B_HL_ENUM  = "b_hl_enum";        //Boolean should highlight enums

    //List of Settings
    private JCheckBox syntaxHighlighting = new JCheckBox("");
    private JCheckBox instructionHighlighting = new JCheckBox("");
    private JCheckBox registerHighlighting = new JCheckBox("");
    private JCheckBox hexadecimalHighlighting = new JCheckBox("");
    private JCheckBox enumHighlighting = new JCheckBox("");


    private void addLabel(String labelTitle, JPanel panel, GridBagConstraints constraints){
        panel.add(new JLabel(labelTitle), constraints);
        constraints.gridy++;
    }

    public PreferencesGUI(){
        setModal(true);
        setTitle("rPeanut: Preferences");

        GridLayout layout = new GridLayout(0,1);
        JPanel contentPane = new JPanel(layout);
        JLabel title = new JLabel("Preferences");
        contentPane.add(title);

        JPanel entryList = new JPanel(new GridBagLayout());
        JPanel highlightSettings = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 0;

        entryList.add(new JLabel("Syntax Highlighting: "), constraints);

        constraints.gridx = 1;
        entryList.add(syntaxHighlighting, constraints);
        constraints.gridx = 0;

        highlightSettings.add(instructionHighlighting, constraints);
        constraints.gridy++;
        highlightSettings.add(registerHighlighting,constraints);
        constraints.gridy++;
        highlightSettings.add(hexadecimalHighlighting,constraints);
        constraints.gridy++;
        highlightSettings.add(enumHighlighting,constraints);

        constraints.gridy = 0;
        constraints.gridx = 1;

        addLabel("Instructions", highlightSettings, constraints);
        addLabel("Registers", highlightSettings, constraints);
        addLabel("Hexadecimal", highlightSettings, constraints);
        addLabel("Enums", highlightSettings, constraints);

        //constraints.gridy = 0;
        //constraints.gridx = 2;

        constraints.gridx = 0;
        constraints.gridwidth = 2;
        entryList.add(highlightSettings, constraints);

        contentPane.add(entryList);

        //Create Confimation pane
        JPanel confirmationPanel = new JPanel();
        JButton confirmButton = new JButton("Update");
        confirmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                savePreferences();
                dispose();
            }
        });
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        confirmationPanel.add(cancelButton);
        confirmationPanel.add(confirmButton);

        contentPane.add(confirmationPanel);
        add(contentPane);
    }

    private void loadPreferences(){
        syntaxHighlighting.setSelected(prefs.getBoolean(B_HL_ALL, true));
        instructionHighlighting.setSelected(prefs.getBoolean(B_HL_INSTR, true));
        registerHighlighting.setSelected(prefs.getBoolean(B_HL_REG, true));
        hexadecimalHighlighting.setSelected(prefs.getBoolean(B_HL_HEX, true));
        enumHighlighting.setSelected(prefs.getBoolean(B_HL_ENUM, true));
    }

    private void savePreferences(){
        prefs.putBoolean(B_HL_ALL, syntaxHighlighting.isSelected());
        prefs.putBoolean(B_HL_INSTR, instructionHighlighting.isSelected());
        prefs.putBoolean(B_HL_REG, registerHighlighting.isSelected());
        prefs.putBoolean(B_HL_HEX, hexadecimalHighlighting.isSelected());
        prefs.putBoolean(B_HL_ENUM, enumHighlighting.isSelected());

        dispose();
    }

    public void display(Preferences prefs){
        this.prefs = prefs;

        loadPreferences();

        pack();
        setVisible(true);
    }
}
