package utils;

import utils.commands.Command;
import utils.commands.CommandList;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhiduo Zhang on 4/17/2015.
 */
public class AutoCompleteListener implements DocumentListener, KeyListener {

    JTextPane textArea;
    private static final String COMMIT_ACTION = "commit";

    private enum Mode { INSERT, COMPLETION };
    private List<String> words;
    private Mode mode = Mode.INSERT;
    private static ResultWindow resultWindow;

    public AutoCompleteListener(JTextPane textArea){
        this.textArea = textArea;
        words = new ArrayList<String>();

        for(Command e : CommandList.getInstance().getCommands()){
            words.add(e.getName());
        }

        resultWindow = new ResultWindow();

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
            resultWindow.setVisible(false);
        } else if(resultWindow.searchResult.isVisible()) {
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                e.consume();
                resultWindow.searchResult.setSelectedIndex((resultWindow.searchResult.getSelectedIndex() + 1) % resultWindow.results.size());
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                e.consume();
                resultWindow.searchResult.setSelectedIndex((resultWindow.searchResult.getSelectedIndex() - 1) % resultWindow.results.size());
            } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                try {
                    System.out.println("Completing");

                    if(resultWindow.searchResult.getSelectedValue().toString().startsWith(resultWindow.prefix)){
                        String insert = resultWindow.searchResult.getSelectedValue().toString().replace(resultWindow.prefix, "");
                        System.out.println("Insert: " + insert);
                        textArea.getDocument().insertString(resultWindow.position, insert, textArea.getCharacterAttributes());
                        textArea.setCaretPosition(resultWindow.position + insert.length());
                        //textArea.moveCaretPosition(resultWindow.position);
                        resultWindow.setVisible(false);
                        e.consume();
                    }

                } catch (BadLocationException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }


    @Override
    public void insertUpdate(DocumentEvent e) {
        updateResultWindow(e,false);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateResultWindow(e, true);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }

    private void updateResultWindow(DocumentEvent e, boolean remove){
        if (e.getLength() != 1) {
            return;
        }

        int pos = e.getOffset();
        if (remove) //Removal will still point to the position of removed character and not the previous character
            pos--;

        String content = null;
        try {
            content =  textArea.getText(0, pos + 1);
        } catch (BadLocationException exception) {
            exception.printStackTrace();
        }

        // Find where the word starts
        int w;
        for (w = pos; w >= 0; w--) {
            assert content != null;
            if (! Character.isLetter(content.charAt(w))) {
                break;
            }
        }

        String prefix = content.substring(w + 1).toLowerCase();

        if (pos - w < 1) {
            // Too few chars
            resultWindow.setVisible(false);
            return;
        }

        SwingUtilities.invokeLater(new CompletionTask(prefix, pos+1));
    }

    public class ResultWindow extends JWindow{
        public JScrollPane pane;
        public JList<Object> searchResult;
        ArrayList<String> results;
        protected int position;
        public String prefix;
        protected DescriptionWindow descriptionWindow;

        public ResultWindow(){
            results = new ArrayList<String>();
            pane = new JScrollPane();
            searchResult = new JList<Object>(words.toArray());
            descriptionWindow = new DescriptionWindow();
        }
        public void initiateDisplay(String prefix, int position){
            this.prefix = prefix;
            results = new ArrayList<String>();
            this.position = position;
            for(String s : words){
                if(s.toLowerCase().startsWith(prefix.toLowerCase())){
                    results.add(s);
                }
            }

            searchResult = new JList<Object>(results.toArray());
            searchResult.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            if (!results.isEmpty()){
                searchResult.setSelectedIndex(0);
            }

            searchResult.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    descriptionWindow.update(searchResult.getSelectedValue().toString());
                }
            });

            addComponentListener(new ComponentListener() {
                @Override
                public void componentResized(ComponentEvent e) {

                }

                @Override
                public void componentMoved(ComponentEvent e) {

                }

                @Override
                public void componentShown(ComponentEvent e) {
                    descriptionWindow.update(searchResult.getSelectedValue().toString());
                }

                @Override
                public void componentHidden(ComponentEvent e) {
                    descriptionWindow.update("");
                }
            });

            pane.setViewportView(searchResult);
            add(pane);
            pack();

            try {
                Rectangle rectangle = textArea.modelToView( textArea.getCaretPosition() );
                int fontHeight = textArea.getFontMetrics(textArea.getFont()).getHeight()+2; //Add 2 for padding
                this.setLocation((int) rectangle.getX(), (int) rectangle.getY()+getHeight()/2+fontHeight);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }

//          this.setPreferredSize(city.getPreferredSize());
            pack();

            if(!results.isEmpty() || (results.size()==1 && results.get(0).equals(prefix))) {
                setVisible(true);
                descriptionWindow.update(searchResult.getSelectedValue().toString());
            } else {
                setVisible(false);
                descriptionWindow.setVisible(false);
            }
        }

        protected class DescriptionWindow extends JWindow{
            //Have description of command next to selection
            JTextArea textArea;
            JScrollPane scrollPane;

            public DescriptionWindow(){
                textArea = new JTextArea(5,20);
                scrollPane = new JScrollPane(textArea);

                add(scrollPane);
                pack();
                setVisible(true);
            }

            public void update(String commandName){
                Command command = CommandList.getInstance().findCommand(commandName);
                if(command != null) {
                    String usage = "Usage: " + command.getUsage();
                    textArea.setText(command.getDescription() + "\n" + usage);
                }
                setLocation(resultWindow.getX() + resultWindow.getWidth(), resultWindow.getY());
                setVisible(resultWindow.isVisible());
            }
        }
    }

    private class CompletionTask implements Runnable {
        String completion;
        int position;

        CompletionTask(String completion, int position) {
            this.completion = completion;
            this.position = position;
        }

        public void run() {
            resultWindow.initiateDisplay(completion, position);
        }
    }
}
