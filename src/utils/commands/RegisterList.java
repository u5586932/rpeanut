package utils.commands;

import java.util.ArrayList;

/**
 * Created by Zhiduo Zhang on 3/31/2015.
 */
public class RegisterList {
    private int registerNum = 7;
    private ArrayList<Register> registerArrayList = new ArrayList<Register>();
    private static RegisterList SINGLETON = new RegisterList();
    public static RegisterList getInstance(){return SINGLETON;}

    private RegisterList(){
        for (int i = 0; i < registerNum; i++) {
            registerArrayList.add(new Register("R"+i, ""));
        }
    }

    public Register getRegister(int registerNum) throws  IndexOutOfBoundsException{
        return registerArrayList.get(registerNum);
    }

    public int getRegisterNum(){
        return registerNum;
    }

    public ArrayList<Register> getRegisterArrayList(){
        return registerArrayList;
    }

}

