package utils.commands;

/**
 * Created by Zhiduo Zhang on 3/31/2015.
 */
public abstract class Command {
    String name;
    String description;
    String usage;

    public String getName(){
        return name;
    }
    public String getDescription(){ return description; }
    public String getUsage(){ return usage;}
}
