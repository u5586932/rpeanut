package utils.commands;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by Zhiduo Zhang on 3/31/2015.
 */
public class CommandList {
    private static ArrayList<Instruction> instructions = new ArrayList<Instruction>();
    private static ArrayList<Enum> enums = new ArrayList<Enum>();
    private static ArrayList<Register> registers = RegisterList.getInstance().getRegisterArrayList();
    private static CommandList SINGLETON = new CommandList();
    public static CommandList getInstance(){return SINGLETON;}

    private CommandList() {
        initInstructionList();
        initEnumList();
    }

    private void initInstructionList(){
        Instruction add = newInstruction(new Instruction("add", "RD <- RS1 + RS2", "RS1", "RS2", "RD"));
        Instruction sub = newInstruction(new Instruction("sub", "RD <- RS1 - RS2", "RS1", "RS2", "RD"));
        Instruction mult = newInstruction(new Instruction("mult", "RD <- RS1 * RS2", "RS1", "RS2", "RD"));
        Instruction div = newInstruction(new Instruction("div", "RD <- RS1 / RS2", "RS1", "RS2", "RD"));
        Instruction mod = newInstruction(new Instruction("mod", "RD <- RS1 % RS2", "RS1", "RS2", "RD"));
        Instruction and = newInstruction(new Instruction("and", "RD <- RS1 & RS2", "RS1", "RS2", "RD"));
        Instruction or = newInstruction(new Instruction("or", " RD <- RS1 | RS2", "RS1", "RS2", "RD"));
        Instruction xor = newInstruction(new Instruction("xor", " RD <- RS1 ^ RS2", "RS1", "RS2", "RD"));
        Instruction neg = newInstruction(new Instruction("neg", "RD <- - RS", "RS", "RD"));
        Instruction not = newInstruction(new Instruction("not", "RD <- ~ RS", "RS", "RD"));
        Instruction move = newInstruction(new Instruction("move", "RD <- RS", "RS", "RD"));
        Instruction call = newInstruction(new Instruction("call", "SP <- SP +1\n" + "mem[SP] <- PC\n" + "PC <- address\n", "address"));
        Instruction returns = newInstruction(new Instruction("return", "PC <- mem[SP]\n" + "SP <- SP-1\n"));
        Instruction trap = newInstruction(new Instruction("trap", "SP <- SP +1\n" + "mem[SP] <- PC\n" + "PC <- 0x0002\n" + "SR <- SR | (1<<1)\n"));
        Instruction jump = newInstruction(new Instruction("jump", "PC <- address", "address", "address"));
        Instruction jumpz = newInstruction(new Instruction("jumpz", "if (R == 0x00000000) {\n" +
                " PC <- address\n" +
                "}", "R", "address", "R", "address"));
        Instruction jumpn = newInstruction(new Instruction("jumpn", "if ((R&0x80000000) !=\n" +
                "0x00000000) {\n" +
                " PC <- address\n" +
                "}", "R", "address", "R", "address"));
        Instruction jumpnz = newInstruction(new Instruction("jumpnz", "if (R != 0x00000000) {\n" +
                " PC <- address\n" +
                "}", "R", "address", "R", "address"));
        Instruction reset = newInstruction(new Instruction("reset", "SR <- SR & ~(1<<BIT)", "bit"));
        Instruction set = newInstruction(new Instruction("set", "SR <- SR | (1<<BIT)", "bit"));
        Instruction push = newInstruction(new Instruction("push", "SP <- SP + 1\n" +
                "mem[SP] <- RS", "RS"));
        Instruction pop = newInstruction(new Instruction("pop", "RD <- mem[SP]\n" +
                "SP <- SP - 1", "RD"));
        Instruction immediateRotate = newInstruction(new Instruction("rotate", "RD <- RS << amount |\n" +
                " RS >>> (32 -\n" +
                "amount)", "#amount", "RS", "RD"));
        Instruction leftRotate = newInstruction(new Instruction("rotate", "RD <- RS << RA |\n" +
                " RS >>> (32 - RA)", "RA", "RS", "RD"));
        Instruction immediateLoad = newInstruction(new Instruction("load", "RD <- ext(value)", "#label or value", "RD"));
        Instruction absoluteLoad = newInstruction(new Instruction("load", "RD <- mem[address]", "address", "RD"));
        Instruction indirectLoad = newInstruction(new Instruction("load", "RD <- mem[RSA]", "RSA", "RD"));
        Instruction displacementLoad = newInstruction(new Instruction("load", "RD <- mem[RSA +\n" +
                "ext(value)]", "RSA", "#value", "RD"));
        Instruction absoluteStore = newInstruction(new Instruction("store", "mem[address] <- RS", "RS", "address"));
        Instruction indirectStore = newInstruction(new Instruction("store", "mem[RDA] <- RS", "RS", "RDA"));
        Instruction displacementStore = newInstruction(new Instruction("store", "> mem[RDA+ext(value)]\n" +
                " <- RS", "#value", "RDA"));
        Instruction halt = newInstruction(new Instruction("halt", "fetch execution stops!"));
    }

    private void initEnumList(){
        Enum MONE = newEnum(new Enum("MONE", ""));
        Enum ONE  = newEnum(new Enum("ONE", ""));
        Enum ZERO = newEnum(new Enum("ZERO", ""));
        Enum block = newEnum(new Enum("block", ""));
    }
    /**
     * Build the regular expression that looks for the whole word of each word that you wish to find.  The "\\b" is the beginning or end of a word boundary.  The "|" is a regex "or" operator.
     * @return
     */
    public Pattern generateInstructionPatterns() {
        StringBuilder sb = new StringBuilder();
        for (Command token : instructions) {
            sb.append("\\b"); // Start of word boundary
            sb.append(token.getName());
            sb.append("\\b|"); // End of word boundary and an or for the next word
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1); // Remove the trailing "|"
        }

        Pattern p = Pattern.compile(sb.toString());

        return p;
    }

    public Pattern generateRegisterPatterns() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < RegisterList.getInstance().getRegisterNum(); i++) {
            Command token = RegisterList.getInstance().getRegister(i);
            sb.append("\\b"); // Start of word boundary
            sb.append(token.getName());
            sb.append("\\b|"); // End of word boundary and an or for the next word
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1); // Remove the trailing "|"
        }

        Pattern p = Pattern.compile(sb.toString());

        return p;
    }

    public Pattern generateHexPattern(){
        String hexPatternString = "\\b0x\\d+\\b";
        return Pattern.compile(hexPatternString);
    }

    public Pattern generateEnumPattern(){
        StringBuilder sb = new StringBuilder();
        for (Enum e : enums) {
            sb.append("\\b"); // Start of word boundary
            sb.append(e.getName());
            sb.append("\\b|"); // End of word boundary and an or for the next word
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1); // Remove the trailing "|"
        }

        Pattern p = Pattern.compile(sb.toString());
        return p;
    }

    private Instruction newInstruction(Instruction instruction){
        instructions.add(instruction);
        return instruction;
    }

    private Enum newEnum(Enum entry){
        enums.add(entry);
        return entry;
    }


    public ArrayList<Command> getCommands(){
        ArrayList<Command> commands = new ArrayList<Command>();
        commands.addAll(instructions);
        commands.addAll(registers);
        commands.addAll(enums);

        return commands;
    }

    public Command findCommand(String commandName){
        for(Command c : getCommands()){
            if (c.name.equals(commandName))
                return c;
        }
        return null;
    }
}
