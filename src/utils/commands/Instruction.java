package utils.commands;

import java.util.ArrayList;

/**
 * Created by Zhiduo Zhang on 3/31/2015.
 */
public class Instruction extends Command{
    ArrayList<String> args = new ArrayList<String>();

    public Instruction(String name, String description, String... args){
        this.name = name;
        this.description = description;
        usage = name;
        for(String arg : args){
            this.args.add(arg);
            usage += (" " + arg);
        }

    }
}
