package utils;

import utils.commands.CommandList;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Zhiduo Zhang on 3/31/2015.
 */
public class TextAid extends DocumentFilter {

    private JTextPane textPane;
    private StyledDocument styledDocument;
    private Preferences preferences;

    private final StyleContext styleContext = StyleContext.getDefaultStyleContext();
    private final AttributeSet blackAttributeSet = styleContext.addAttribute(styleContext.getEmptySet(), StyleConstants.Foreground, Color.BLACK);
    private final AttributeSet instructionAttributeSet = styleContext.addAttribute(styleContext.getEmptySet(), StyleConstants.Foreground, Color.BLUE);
    private final AttributeSet registerAttributeSet = styleContext.addAttribute(styleContext.getEmptySet(), StyleConstants.Foreground, Color.CYAN);
    private final AttributeSet hexAttributeSet = styleContext.addAttribute(styleContext.getEmptySet(), StyleConstants.Foreground, Color.GREEN);
    private final AttributeSet enumAttributeSet = styleContext.addAttribute(styleContext.getEmptySet(), StyleConstants.Foreground, Color.MAGENTA);

    // Use a regular expression to find the words you are looking for
    //Pattern pattern = buildPattern();
    Pattern instructionPattern = CommandList.getInstance().generateInstructionPatterns();
    Pattern registerPattern = CommandList.getInstance().generateRegisterPatterns();
    Pattern hexPattern = CommandList.getInstance().generateHexPattern();
    Pattern enumPattern = CommandList.getInstance().generateEnumPattern();

    public TextAid(JTextPane textPane, Preferences preferences) {
        this.textPane = textPane;
        styledDocument = textPane.getStyledDocument();
        this.preferences = preferences;
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String text, AttributeSet attributeSet) throws BadLocationException {
        super.insertString(fb, offset, text, attributeSet);

        handleTextChanged();
    }

    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
        super.remove(fb, offset, length);

        handleTextChanged();
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attributeSet) throws BadLocationException {
        super.replace(fb, offset, length, text, attributeSet);

        handleTextChanged();
    }

    /**
     * Runs your updates later, not during the event notification.
     */
    private void handleTextChanged()
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                updateTextStyles();
            }
        });
    }

    private void updateTextStyles()
    {
        // Clear existing styles
        styledDocument.setCharacterAttributes(0, textPane.getText().length(), blackAttributeSet, true);

        //Highlight each type of command

        if(preferences.getBoolean(PreferencesGUI.B_HL_ALL, true)) {
            if(preferences.getBoolean(PreferencesGUI.B_HL_INSTR, true))
                highlight(instructionPattern, instructionAttributeSet);
            if(preferences.getBoolean(PreferencesGUI.B_HL_REG, true))
                highlight(registerPattern, registerAttributeSet);
            if(preferences.getBoolean(PreferencesGUI.B_HL_HEX, true))
                highlight(hexPattern, hexAttributeSet);
            if (preferences.getBoolean(PreferencesGUI.B_HL_ENUM, true))
                highlight(enumPattern, enumAttributeSet);
        }
    }

    private void highlight(Pattern pattern, AttributeSet attributeSet){
        // Look for tokens and highlight them
        Matcher matcher = pattern.matcher(textPane.getText().replaceAll("\r\n","\n"));
        while (matcher.find()) {
            // Change the color of recognized tokens
            styledDocument.setCharacterAttributes(matcher.start(), matcher.end() - matcher.start(), attributeSet, false);
        }
    }
}

